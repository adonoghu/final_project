#!/usr/bin/env python3

import json

class meteor_db(object):

    def __init__(self):
        self.meteors   = dict()

    def load_meteors(self, meteor_file):
        f = open(meteor_file)
        meteor_data = json.load(f)

        gmeteor_data = [meteor for meteor in meteor_data if 'year' in meteor and 'mass' in meteor and 'geolocation' in meteor]
        rmeteor_data = [meteor for meteor in gmeteor_data if int(meteor['year'][0:4]) > 1999]
        
        for meteor in rmeteor_data:
            
            lat = meteor['geolocation']['latitude']
            lon = meteor['geolocation']['longitude']
            index = (lat, lon)
            
            meteor_dict = dict()
            meteor_dict['name'] = meteor['name']
            meteor_dict['id'] = meteor['id']
            meteor_dict['nametype'] = meteor['nametype']
            meteor_dict['recclass'] = meteor['recclass']
            meteor_dict['mass'] = meteor['mass']
            meteor_dict['fall'] = meteor['fall']
            meteor_dict['year'] = meteor['year'][0:4] # relevant year digits
            
            self.meteors[index] = meteor_dict
        
        f.close()

    def get_meteor(self, lat, lon):
        if (lat, lon) in self.meteors:
            return self.meteors[(lat, lon)]

        return None

    def get_meteors_y(self, year):
        return [meteor for meteor in self.meteors.values() if meteor['year'] == year]

    def delete_meteor(self, lat, lon):
        if (lat, lon) in self.meteors:
            del self.meteors[(lat, lon)]

    def print_meteors(self):
        for location, details in self.meteors.items():
            print("Location: {} \nDetails: {}".format(location, details))

if __name__ == "__main__":
    mdb = meteor_db()
    
    mdb.load_meteors('meteors/meteor_data.json')
    mdb.print_meteors()

