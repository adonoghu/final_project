# Programming Paradigms Final Project

Final Project for Programming Paradigms CSE 30332

## Group Members

- Aemile Donoghue (adonoghu)
- Carson Lance (clance1)
- Connor Kuse (ckuse1)
- Jamie Zhang ()

## General Overview

Our project is a web application which takes data from various sources of natural disasters and occurrences (e.g. Forest Fires, Tornadoes, Meteor Landings, etc.) over the last 20 years.
This data is then used to generate a visual mapping of historical sites of natural importance or correlation with regard to natural disasters. The data will also be used to make
predictions on times and areas that these occurrences are likely to happen again.

## Usage of API

The main file, _natural_disasters_database.py, is a general class file that imports and uses functions specific to other class files. These files are as listed:

 - tornado_db.py
 - meteor_db.py
 - fires_db.py

As of completion of the first project milestone, this API is able to make queries through the data based on various factors such as state of occurrence, year/month of occurrence,
and type of occurrence. Since returning certain data based on user queries with specified conditions is a large majority of our project, we found it sufficient to merely include these
functions as of this first milestone. However, there are also quality of life functions such as resetting, deleting, and printing as well.

## Running Tests

To test the API and make sure that all functions are working correctly, the test_api.py file can be run. This will run a series of unit tests on the class files which will test all
used functions. Simply type 'python3 test_api.py' to use.
