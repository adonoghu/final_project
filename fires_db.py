#############################################
#               fires_db.py                #
#   This api allows access to the data      #
#   from all of the wildfires across the    #
#   United States from 2000 - 2017          #
#                                           #
#   The database is setup as a dictionary   #
#   with the format below:                  #
#                                           #
#   state_abrev: {                          #
#               year: {                     #
#                   index: {                #
#                          'latitude':____  #
#                          'longitude':____ #
#                          'start_date':___ #
#                          'end_date':____  #
#                          'category':____  #
#                   }                       #
#               }                           #
#   }                                       #
#############################################

import pandas as pd

class FiresApi:
    fires = {}
    def __init__(self):
        self.fill_data()

    # Fills the data
    def fill_data(self):
        # Create dictionary of fires
        df = pd.read_csv('fires/fires.csv')
        count = 0
        for index, fire in df.iterrows():
            if fire[6] not in self.fires.keys():
                new_dict = {fire[6]: {
                                fire[1]: {0: {
                                        'latitude': fire[2],
                                        'longitude': fire[3],
                                        'start_date': fire[4],
                                        'end_date': fire[5],
                                        'category': fire[7]
                                        }
                                    }
                                }
                            }
                self.fires.update(new_dict)
            elif fire[1] not in self.fires.get(fire[6]).keys():
                new_dict = {fire[1]: {0: {
                                        'latitude': fire[2],
                                        'longitude': fire[3],
                                        'start_date': fire[4],
                                        'end_date': fire[5],
                                        'category': fire[7]
                                        }
                                    }
                               }
                self.fires.get(fire[6]).update(new_dict)
            else:
                num = list(self.fires.get(fire[6]).get(fire[1]).keys())[-1]
                num = num + 1
                new_dict = {num: {'latitude': fire[2],
                                'longitude': fire[3],
                                'start_date': fire[4],
                                'end_date': fire[5],
                                'category': fire[7]}
                           }
                self.fires.get(fire[6]).get(fire[1]).update(new_dict)

    # Returns all the data
    def get_all_data(self):
        return self.fires

    # Returns a dictionary of all of the fire data for the inputted state
    def get_state_data(self, state):
        return self.fires.get(state)

    # Returns a dictionary of all of the state data for a specific year
    def get_year_state_data(self, state, year):
        return self.fires.get(state).get(year)

    # Returns a dictionary of all of the data for a specific year
    def get_year_data(self, year):
        year_dict = {}
        for state in list(self.fires.keys()):
            new_dict = {state: self.fires.get(state).get(year)}
            year_dict.update(new_dict)

        return year_dict

    # Clears the entire dictionary
    def clear_data(self):
        self.fires.clear()

    # Resets the data from the original data set
    def reset_data(self):
        self.fires.clear()
        self.fill_data()


    # Returns all of the category data for a specific year
    def get_year_category_data(self, year, category):
        category_dict = {}

        for state in list(self.fires.keys()):
            state_dict = self.fires.get(state).get(year)

            for index in list(state_dict.keys()):
                if state_dict.get(index).get('category') == category:
                    new_dict = {state: self.fires.get(state).get(year)}
                    category_dict.update(new_dict)

        return category_dict

    # Returns all of the category data for a specific state
    def get_state_category_data(self, state, category):
        category_dict = {}

        for year in list(self.fires.get(state).keys()):
            state_dict = self.fires.get(state).get(year)

            for index in list(state_dict.keys()):
                if state_dict.get(index).get('category') == category:
                    if year not in category_dict.keys():
                        new_dict = {year: {0: self.fires.get(state).get(year).get(index)}}
                        category_dict.update(new_dict)
                    else:
                        num = list(category_dict.get(year).keys())[-1]
                        num = num + 1
                        new_dict = {num: self.fires.get(state).get(year).get(index)}
                        category_dict.get(year).update(new_dict)

        return category_dict

    # Allows for the setting of a data point in the dictionary
    def set_datapoint(self, state, year, category, latitude, longitude, start_date, end_date):
        num = list(self.fires.get(state).get(year).keys())[-1]
        new_dict = {num: {
                        'latitude': latitude,
                        'longitude': longitude,
                        'start_date': start_date,
                        'end_date': end_date,
                        'category': category
                        }
                    }
        self.fires.get(state).get(year).update(new_dict)
