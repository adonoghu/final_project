#!/usr/bin/env python3

import json
import pprint
from tornado_db import tornado_db
from meteor_db import meteor_db


class _natural_disasters_database(object):

    def __init__(self):
        self.tornadoes = tornado_db()
        self.tornadoes.load_tornadoes('tornadoes/tornadoes.json')
        self.meteors = meteor_db()
        self.meteors.load_meteors('meteors/meteor_data.json')

    def reset_data(self):
        self.tornadoes = tornado_db()
        self.tornadoes.load_tornadoes('tornadoes/tornadoes.json')
        self.meteors = meteor_db()
        self.meteors.load_meteors('meteors/meteor_data.json')

if __name__ == "__main__":
    nd_db = _natural_disasters_database()
