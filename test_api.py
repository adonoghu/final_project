#!/usr/bin/env python3

from _natural_disasters_database import _natural_disasters_database
import unittest

class TestNDDatabase(unittest.TestCase):
    ''' Unit tests for Paradigms OO API '''

    nd_db = _natural_disasters_database()

    def reset_data(self):
        
        self.nd_db.reset_data()

    def test_meteors(self):
        
        self.reset_data()

        meteors = self.nd_db.meteors.get_meteors_y('2001')
        self.assertEqual(meteors[1]['name'], 'Beni M\'hira')

        meteor = self.nd_db.meteors.get_meteor('34.45', '132.38333')
        self.assertEqual(meteor['name'], 'Hiroshima')
        self.assertEqual(meteor['mass'], '414')

        self.nd_db.meteors.delete_meteor('34.45', '132.38333')
        meteor = self.nd_db.meteors.get_meteor('34.45', '132.38333')
        self.assertEqual(meteor, None)

        self.reset_data()
        
    def test_tornadoes(self):

        self.reset_data()
        print(self.nd_db.tornadoes.get_tornadoes_year('2015'))
        self.assertEqual((tornadoes[1])['state'], 'OKLAHOMA')

        tornado = self.nd_db.tornadoes.get_tornado('34.89', '-98.3159')
        self.assertEqual(tornado['begin_year'], '2015')
        self.assertEqual(tornado['begin_month'], '5')

        self.nd_db.tornadoes.set_tornado('35.6000', '-100.000', '35.0000', '-100.000', '2015', '1', '1', '2015', \
                                        '1', '1', '1800', '1820', 'TEXAS', '000000', 'TEST', 'EF3')
        tornado = self.nd_db.tornadoes.get_tornado('35.6000', '-100.000')
        self.assertEqual(tornado['name'], 'TEST')

        tornadoes_2015 = self.nd_db.tornadoes.get_tornadoes_year(2015)
        texas_tornadoes = self.nd_db.tornadoes.get_tornadoes_state('TEXAS')
        texas_2015_tornadoes = self.nd_db.tornadoes.get_tornadoes_year_state(2015, 'TEXAS')
        self.assertEqual(len(tornadoes_2015), 1316)
        self.assertEqual(len(texas_tornadoes), 257)
        self.assertEqual(len(texas_2015_tornadoes), 257)


        self.nd_db.tornadoes.delete_tornado()
        tornado = self.nd_db.tornadoes.get_tornado('34.89', '-98.3159')
        self.assertEqual(tornado,None)

if __name__ == '__main__':
    unittest.main()
